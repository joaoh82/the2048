//
//  main.m
//  2048
//
//  Created by Nurt  on 16.03.14.
//  Copyright (c) 2014 Nurt Games. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char * argv[])
{
  @autoreleasepool {
      return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
  }
}
