//
//  AppHelper.h
//  Donde Comer
//
//  Created by Nurt  on 09.02.14.
//  Copyright (c) 2014 Nurt Games. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AppHelper : NSObject

+ (BOOL)isPhone;

@end
