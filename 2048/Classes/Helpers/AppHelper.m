//
//  AppHelper.m
//  Donde Comer
//
//  Created by Nurt  on 09.02.14.
//  Copyright (c) 2014 Nurt Games. All rights reserved.
//

#import "AppHelper.h"

@implementation AppHelper

+ (BOOL)isPhone {
    return UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone;
}

@end
