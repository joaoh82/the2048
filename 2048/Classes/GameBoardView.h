//
//  CCGameBoard.h
//  2048
//
//  Created by Nurt  on 3/24/14.
//  Copyright (c) 2014 Nurt Games. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GameBoardView : UIView

+ (instancetype)gameboardWithDimension:(NSUInteger)dimension
                             cellWidth:(CGFloat)width
                           cellPadding:(CGFloat)padding
                       backgroundColor:(UIColor *)backgroundColor
                       foregroundColor:(UIColor *)foregroundColor;

- (void)insertTileAtIndexPath:(NSIndexPath *)path
                    withValue:(NSUInteger)value;

- (void)moveTileOne:(NSIndexPath *)startA
            tileTwo:(NSIndexPath *)startB
        toIndexPath:(NSIndexPath *)end
          withValue:(NSUInteger)value;

- (void)moveTileAtIndexPath:(NSIndexPath *)start
                toIndexPath:(NSIndexPath *)end
                  withValue:(NSUInteger)value;

@end
