//
//  Tile.h
//  2048
//
//  Created by Nurt  on 3/18/14.
//  Copyright (c) 2014 Nurt Games. All rights reserved.
//

@interface TileView : UIView

@property (nonatomic) NSInteger tileValue;

+ (instancetype)tileForPosition:(CGPoint)position
                     sideLength:(CGFloat)side
                          value:(NSUInteger)value;

@end