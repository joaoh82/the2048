//
//  ViewController.m
//  2048
//
//  Created by Nurt  on 16.03.14.
//  Copyright (c) 2014 Nurt Games. All rights reserved.
//

#import <AudioToolbox/AudioToolbox.h>
#import <Social/Social.h>

#import "GameViewController.h"
#import "GameboardView.h"
#import "GameModel.h"
#import "GameCenterManager.h"
#import "GameEndViewController.h"
#import "AppHelper.h"

#import "Chartboost.h"

#define LeaderBoardName @"br.com.nurt.the2048.leaderboard"

#define UIColorFromRGB(rgbValue) [UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 green:((float)((rgbValue & 0xFF00) >> 8))/255.0 blue:((float)(rgbValue & 0xFF))/255.0 alpha:1.0]

@interface GameViewController ()
<
GameModelProtocol,
UIActionSheetDelegate,
GameCenterManagerDelegate,
GameEndDelegate
> {
    SystemSoundID attachSound;
    SystemSoundID newTileSound;
}

@property (nonatomic, assign) IBOutlet UIView *gameBoardBackgroundView;
@property (nonatomic, strong) GameBoardView *gameboard;
@property (nonatomic, strong) GameModel *model;

@property (nonatomic, assign) NSInteger score;

@property (nonatomic, assign) IBOutlet UIButton *gameCenterButton;
@property (nonatomic, assign) IBOutlet UIButton *replayButton;
@property (nonatomic, assign) IBOutlet UIButton *shareButton;

@property (nonatomic, assign) IBOutlet UILabel *currentScore;
@property (nonatomic, assign) IBOutlet UILabel *bestScoreLabel;

@property (nonatomic, weak) IBOutlet UIView *howToView;

- (IBAction)onGameCenterPress:(id)sender;
- (IBAction)onReplayPress:(id)sender;
- (IBAction)onSharePress:(id)sender;

- (void)onHowToTap:(id)sender;

@property (nonatomic) BOOL moveFlag;

@end

@implementation GameViewController

@synthesize howToView;

- (void)onHowToTap:(id)sender{
//    [howToView removeFromSuperview];
    
    [UIView animateWithDuration:0.5
                          delay:0.0
                        options: UIViewAnimationOptionCurveLinear
                     animations:^{
                         howToView.alpha = 0;
                     }completion:^(BOOL finished){
                         [howToView removeFromSuperview];
                     }];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.navigationController setNavigationBarHidden:YES animated:NO];
    
    self.moveFlag = NO;
    
    [self _startNewGame];
    
    [self _setupGestureRecognizers];
    
    self.currentScore.font = [UIFont fontWithName:@"AvenirNext-Bold" size:22];
    self.currentScore.layer.cornerRadius = 5.0f;
    
    [self _setScore:0];
    
    NSURL *soundURL = [[NSBundle mainBundle] URLForResource:@"attach"
                                              withExtension:@"mp3"];
    AudioServicesCreateSystemSoundID(CFBridgingRetain(soundURL), &attachSound);
    
    NSURL *soundURL2 = [[NSBundle mainBundle] URLForResource:@"new_tile"
                                               withExtension:@"mp3"];
    AudioServicesCreateSystemSoundID(CFBridgingRetain(soundURL2), &newTileSound);
    
    [[GameCenterManager sharedManager] setDelegate:self];
    
    [UIView animateWithDuration:1.0
                          delay:0.0
                        options: UIViewAnimationOptionCurveLinear
                     animations:^{
                         howToView.alpha = 0.7;
                     }completion:^(BOOL finished){
                         howToView.alpha = 0.7;
                     }];
    
    UITapGestureRecognizer *tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onHowToTap:)];
    [howToView addGestureRecognizer:tapGestureRecognizer];
}

#pragma mark — IBActions

- (IBAction)omMoreAppsPress:(id)sender {
    [[Chartboost sharedChartboost] showMoreApps];
}

- (IBAction)onGameCenterPress:(id)sender {
    if ([[GameCenterManager sharedManager] checkGameCenterAvailability]) {
        [[GameCenterManager sharedManager] presentLeaderboardsOnViewController:self];
    }else{
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"The 2048" message:@"You are not logged in with Game Center." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
    }
}

- (IBAction)onReplayPress:(id)sender {
    [self _startNewGame];
}

- (IBAction)onSharePress:(id)sender {
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:@"Share"
                                                             delegate:self
                                                    cancelButtonTitle:@"Cancel"
                                               destructiveButtonTitle:nil
                                                    otherButtonTitles:@"Facebook", @"Twitter", nil];
    
    [actionSheet showInView:[UIApplication sharedApplication].keyWindow];
}

#pragma mark — Move Methods

- (void)moveUp {
    if ([self.model performMoveInDirection:MoveDirectionUp]) {
        AudioServicesPlaySystemSound(newTileSound);
        [self followUp];
    }
}

- (void)moveDown {
    if ([self.model performMoveInDirection:MoveDirectionDown]) {
        AudioServicesPlaySystemSound(newTileSound);
        [self followUp];
    }
}

- (void)moveLeft {
    if ([self.model performMoveInDirection:MoveDirectionLeft]) {
        AudioServicesPlaySystemSound(newTileSound);
        [self followUp];
    }
}

- (void)moveRight {
    if ([self.model performMoveInDirection:MoveDirectionRight]) {
        AudioServicesPlaySystemSound(newTileSound);
        [self followUp];
    }
}

- (void)followUp {
    // This is the earliest point the user can win
    if ([self.model userHasWon]) {
        [self _showGameEndScreenWitnWin:YES];
    }
    else {
        NSInteger rand = arc4random_uniform(10);
        if (rand == 1) {
            [self.model insertAtRandomLocationTileWithValue:2];
        }
        else {
            [self.model insertAtRandomLocationTileWithValue:2];
        }
        // At this point, the user may lose
        if ([self.model userHasLost]) {
            [self _showGameEndScreenWitnWin:NO];
        }
    }
}

#pragma mark — GameEndDelegate

- (void)shouldStartNewGame {
    [self _startNewGame];
}

#pragma mark - Protocol

- (void)moveTileFromIndexPath:(NSIndexPath *)fromPath toIndexPath:(NSIndexPath *)toPath newValue:(NSUInteger)value {
    [self.gameboard moveTileAtIndexPath:fromPath toIndexPath:toPath withValue:value];
}

- (void)moveTileOne:(NSIndexPath *)startA tileTwo:(NSIndexPath *)startB toIndexPath:(NSIndexPath *)end newValue:(NSUInteger)value {
    [self.gameboard moveTileOne:startA tileTwo:startB toIndexPath:end withValue:value];
}

- (void)insertTileAtIndexPath:(NSIndexPath *)path value:(NSUInteger)value {
    [self.gameboard insertTileAtIndexPath:path withValue:value];
}

- (void)playerLost {
    // TODO
}

- (void)playerWonWithTile:(NSIndexPath *)tilePath {
    // TODO
}

- (void)newScore:(NSInteger)score {
    NSLog(@"score: %i", score);
    
    [self _setScore:score];
    
    AudioServicesPlaySystemSound(attachSound);
}

#pragma mark - Actionsheet delegate methods

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
    SLComposeViewController *shareSheet;
    switch (buttonIndex) {
        case 0: {
            // Facebook sharing
            shareSheet = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeFacebook];
            break;
        }
        case 1: {
            // Twitter sharing
            shareSheet = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeTwitter];
            break;
        }
            
        default: {
            return;
        }
    }
    if (shareSheet) {
        NSString *shareString = [NSString stringWithFormat:@"I've got %i on THE 2048 game!", self.score];
        
        [shareSheet setInitialText:shareString];
        [shareSheet addImage:[UIImage imageNamed:@"Icon-FB.png"]];
        [shareSheet addURL:[NSURL URLWithString:ITUNES_CONNECT_URL]];
        [self presentViewController:shareSheet animated:YES completion:nil];
    }
}

#pragma mark - GameCenter Manager Delegate

- (void)gameCenterManager:(GameCenterManager *)manager authenticateUser:(UIViewController *)gameCenterLoginController {
    [self presentViewController:gameCenterLoginController animated:YES completion:^{
        NSLog(@"Finished Presenting Authentication Controller");
    }];
}

- (void)gameCenterManager:(GameCenterManager *)manager reportedScore:(GKScore *)score withError:(NSError *)error {
    if (!error) {
        NSLog(@"GCM Reported Score: %@", score);
    } else {
        NSLog(@"GCM Error while reporting score: %@", error);
    }
}

- (void)gameCenterManager:(GameCenterManager *)manager didSaveScore:(GKScore *)score2 {
    NSLog(@"Saved GCM Score with value: %lld", score2.value);
}

- (void)gameCenterManager:(GameCenterManager *)manager error:(NSError *)error {
    NSLog(@"GCM Error: %@", error);
}

#pragma mark — Private Methods

- (void)_startNewGame {
    [self.gameboard removeFromSuperview];
    
    UIColor *backgroundColor = [UIColor colorWithRed:0.55 green:0.52 blue:0.29 alpha:1.0]; //UIColorFromRGB(0x8E8E93); //[UIColor clearColor];
    
    CGFloat cellWidth = [AppHelper isPhone] ? 60 : 110;
    CGFloat cellPadding = [AppHelper isPhone] ? 12 : 24;
    GameBoardView *gameboard = [GameBoardView gameboardWithDimension:4
                                                           cellWidth:cellWidth
                                                         cellPadding:cellPadding
                                                     backgroundColor:backgroundColor
                                                     foregroundColor:[UIColor darkGrayColor]];
    gameboard.layer.cornerRadius = 5.0;
    
    [self.gameBoardBackgroundView addSubview:gameboard];
    
    self.gameboard = gameboard;
    
    GameModel *model = [GameModel gameModelWithDimension:4 winValue:2048 delegate:self];
    
    [model insertAtRandomLocationTileWithValue:2];
    [model insertAtRandomLocationTileWithValue:2];
    
    self.model = model;
}

- (void)_showGameEndScreenWitnWin:(BOOL)didWin {
    GameEndViewController *gameEndVC = [self.storyboard instantiateViewControllerWithIdentifier:@"GameEndVC"];
    
    gameEndVC.delegate = self;
    gameEndVC.score = self.score;
    gameEndVC.didWin = didWin;
    
    [self presentViewController:gameEndVC animated:YES completion:nil];
}

- (void)_setScore:(NSInteger)score {
    
    self.score = score;
    self.currentScore.text = [NSString stringWithFormat:@"Score: %i", score];
    
    if (![[NSUserDefaults standardUserDefaults] integerForKey:@"highscore"]) {
        
        [[NSUserDefaults standardUserDefaults] setInteger:self.score forKey:@"highscore"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
    } else {
        
        NSInteger pastScore = [[NSUserDefaults standardUserDefaults] integerForKey:@"highscore"];
        if (self.score > pastScore) {
            
            [[NSUserDefaults standardUserDefaults] setInteger:self.score forKey:@"highscore"];
            [[NSUserDefaults standardUserDefaults] synchronize];
            [self _reportScoreToGameCenter];  // ADD THIS LINE
            
        }
    }
    
    NSInteger bestScore = [[NSUserDefaults standardUserDefaults] integerForKey:@"highscore"];
    self.bestScoreLabel.text = [NSString stringWithFormat:@"Best: %i", bestScore];
}

- (void)_reportScoreToGameCenter {
    [[GameCenterManager sharedManager] saveAndReportScore:self.score
                                              leaderboard:LeaderBoardName
                                                sortOrder:GameCenterSortOrderHighToLow];
}

- (void)_setupGestureRecognizers {
    UISwipeGestureRecognizer* swipeLeftRecognizer = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(moveLeft)];
    [swipeLeftRecognizer setDirection:UISwipeGestureRecognizerDirectionLeft];
    [self.view addGestureRecognizer:swipeLeftRecognizer];
    
    UISwipeGestureRecognizer* swipeRightRecognizer = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(moveRight)];
    [swipeRightRecognizer setDirection:UISwipeGestureRecognizerDirectionRight];
    [self.view addGestureRecognizer:swipeRightRecognizer];
    
    UISwipeGestureRecognizer* swipeUpRecognizer = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(moveUp)];
    [swipeUpRecognizer setDirection:UISwipeGestureRecognizerDirectionUp];
    [self.view addGestureRecognizer:swipeUpRecognizer];
    
    UISwipeGestureRecognizer* swipeDownRecognizer = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(moveDown)];
    [swipeDownRecognizer setDirection:UISwipeGestureRecognizerDirectionDown];
    [self.view addGestureRecognizer:swipeDownRecognizer];
}

@end

@interface GameViewControllerPad ()

@property (nonatomic, assign) IBOutlet NSLayoutConstraint *topConstraint;

@end

@implementation GameViewControllerPad

- (void)viewWillAppear:(BOOL)animated {
    [self _changeConstraintForInterfaceOrientation:self.interfaceOrientation];
}

- (void)willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration {
    [self _changeConstraintForInterfaceOrientation:toInterfaceOrientation];
}

- (void)_changeConstraintForInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    if (UIInterfaceOrientationIsPortrait(interfaceOrientation)) {
        self.topConstraint.constant = 178;
    }
    else {
        self.topConstraint.constant = 0;
    }
}

@end

