//
//  GameEndViewController.m
//  2048
//
//  Created by Nurt  on 3/25/14.
//  Copyright (c) 2014 Nurt Games. All rights reserved.
//

#import <Social/Social.h>
#import "GameEndViewController.h"
#import "Chartboost.h"
#import "AppHelper.h"

@interface GameEndViewController ()
<
UIActionSheetDelegate
>

@property (nonatomic, assign) IBOutlet UILabel *winLabel;
@property (nonatomic, assign) IBOutlet UILabel *scoreLabel;

@property (nonatomic, assign) IBOutlet UIButton *shareButton;
@property (nonatomic, assign) IBOutlet UIButton *replayButton;
@property (nonatomic, assign) IBOutlet UIButton *moreAppsButton;

- (IBAction)openShare:(id)sender;
- (IBAction)replayGame:(id)sender;
- (IBAction)moreApps:(id)sender;

@end

@implementation GameEndViewController

- (void)viewDidLoad {
  [super viewDidLoad];
  
  self.winLabel.font = [UIFont fontWithName:@"AvenirNext-Heavy" size:30];
  self.winLabel.backgroundColor = [UIColor colorWithRed:0.55 green:0.52 blue:0.29 alpha:1.0];
  self.winLabel.textColor = [UIColor whiteColor];
  self.winLabel.text = self.didWin ? @"You win!" : @"You lose!";
  self.winLabel.layer.cornerRadius = 5.0f;
  
  self.scoreLabel.font = [UIFont fontWithName:@"AvenirNext-Heavy" size:30];
  self.scoreLabel.backgroundColor = [UIColor colorWithRed:0.55 green:0.52 blue:0.29 alpha:1.0];
  self.scoreLabel.textColor = [UIColor whiteColor];
  self.scoreLabel.text = [NSString stringWithFormat:@"Score: %i", self.score];
  self.scoreLabel.layer.cornerRadius = 5.0f;

  self.shareButton.titleLabel.font = [UIFont fontWithName:@"Helvetica-Bold" size:23];
  self.shareButton.backgroundColor = [UIColor colorWithRed:0.55 green:0.52 blue:0.29 alpha:1.0];
  self.shareButton.titleLabel.textColor = [UIColor whiteColor];
  self.shareButton.layer.cornerRadius = 5.0f;

  self.replayButton.titleLabel.font = [UIFont fontWithName:@"Helvetica-Bold" size:23];
  self.replayButton.backgroundColor = [UIColor colorWithRed:0.55 green:0.52 blue:0.29 alpha:1.0];
  self.replayButton.titleLabel.textColor = [UIColor whiteColor];
  self.replayButton.layer.cornerRadius = 5.0f;
  
  self.moreAppsButton.titleLabel.font = [UIFont fontWithName:@"Helvetica-Bold" size:23];
  self.moreAppsButton.backgroundColor = [UIColor colorWithRed:0.55 green:0.52 blue:0.29 alpha:1.0];
  self.moreAppsButton.titleLabel.textColor = [UIColor whiteColor];
  self.moreAppsButton.layer.cornerRadius = 5.0f;
}

- (IBAction)openShare:(id)sender {
  UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:@"Share"
                                                           delegate:self
                                                  cancelButtonTitle:@"Cancel"
                                             destructiveButtonTitle:nil
                                                  otherButtonTitles:@"Facebook", @"Twitter", nil];
  
  [actionSheet showInView:[UIApplication sharedApplication].keyWindow];
}

- (IBAction)replayGame:(id)sender {
  [self dismissViewControllerAnimated:YES completion:nil];
  [self.delegate shouldStartNewGame];
}

- (IBAction)moreApps:(id)sender {
  [[Chartboost sharedChartboost] showMoreApps];
}

#pragma mark - Actionsheet delegate methods

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
  SLComposeViewController *shareSheet;
  switch (buttonIndex) {
    case 0: {
      // Facebook sharing
      shareSheet = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeFacebook];
      break;
    }
    case 1: {
      // Twitter sharing
      shareSheet = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeTwitter];
      break;
    }
      
    default: {
      return;
    }
  }
  if (shareSheet) {
    NSString *shareString = [NSString stringWithFormat:@"I've got %i on THE 2048 game!", self.score];
    
    [shareSheet setInitialText:shareString];
    [shareSheet addImage:[UIImage imageNamed:@"Icon-FB.png"]];
    [shareSheet addURL:[NSURL URLWithString:ITUNES_CONNECT_URL]];
    [self presentViewController:shareSheet animated:YES completion:nil];
  }
}

@end
