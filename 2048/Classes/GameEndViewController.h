//
//  GameEndViewController.h
//  2048
//
//  Created by Nurt  on 3/25/14.
//  Copyright (c) 2014 Nurt Games. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol GameEndDelegate <NSObject>

- (void)shouldStartNewGame;

@end

@interface GameEndViewController : UIViewController

@property (nonatomic, assign) id<GameEndDelegate> delegate;

@property (nonatomic, assign) BOOL didWin;
@property (nonatomic, assign) NSInteger score;

@end
