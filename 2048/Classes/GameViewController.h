//
//  ViewController.h
//  2048
//
//  Created by Nurt  on 16.03.14.
//  Copyright (c) 2014 Nurt Games. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GameViewController : UIViewController

@end

@interface GameViewControllerPad : GameViewController

@end
