//
//  Tile.m
//  2048
//
//  Created by Nurt  on 3/18/14.
//  Copyright (c) 2014 Nurt Games. All rights reserved.
//

#import "TileView.h"

 #define UIColorFromRGB(rgbValue) [UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 green:((float)((rgbValue & 0xFF00) >> 8))/255.0 blue:((float)(rgbValue & 0xFF))/255.0 alpha:1.0]

@interface TileView ()

@property (nonatomic, readonly) UIColor *defaultBackgroundColor;
@property (nonatomic, readonly) UIColor *defaultNumberColor;

@property (nonatomic, strong) UILabel *numberLabel;
@property (nonatomic) NSUInteger value;
@end

@implementation TileView

+ (instancetype)tileForPosition:(CGPoint)position
                     sideLength:(CGFloat)side
                          value:(NSUInteger)value {
  TileView *tile = [[[self class] alloc] initWithFrame:CGRectMake(position.x,
                                                                  position.y,
                                                                  side,
                                                                  side)];
  tile.tileValue = value;
  //tile.backgroundColor = tile.defaultBackgroundColor;
  //tile.numberLabel.textColor = tile.defaultNumberColor;
  tile.value = value;
  return tile;
}

- (id)initWithFrame:(CGRect)frame {
  self = [super initWithFrame:frame];
  if (!self) return nil;
  
  UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0,
                                                             0,
                                                             frame.size.width,
                                                             frame.size.height)];
  label.textAlignment = NSTextAlignmentCenter;
    label.font = [UIFont fontWithName:@"AvenirNext-Bold" size:22];

  label.backgroundColor = [UIColor clearColor];
  
  [self addSubview:label];
  self.numberLabel = label;
  self.layer.cornerRadius = 3;
  return self;
}

- (void)setTileValue:(NSInteger)tileValue {
  _tileValue = tileValue;
  self.numberLabel.text = [@(tileValue) stringValue];

  self.backgroundColor = [self tileColorForValue:tileValue];
  self.numberLabel.textColor = [self numberColorForValue:tileValue];
  
  self.value = tileValue;
}

- (UIColor *)defaultBackgroundColor {
  return UIColorFromRGB(0x8E8E93);
}

- (UIColor *)defaultNumberColor {
  return [UIColor darkGrayColor];
}

- (UIColor *)tileColorForValue:(NSUInteger)value {
  switch (value) {
    case 1:
      return UIColorFromRGB(0xFFCC00); // light gray
    case 2:
      return UIColorFromRGB(0x8E8E93); // gray
    case 4:
      return UIColorFromRGB(0xC7C7C7); // gark gray
    case 8:
      return UIColorFromRGB(0xFF2D55); // red
    case 16:
      return UIColorFromRGB(0xFF9500); // orange
    case 32:
      return UIColorFromRGB(0xFF5E3A); // orange-yellow
    case 64:
      return UIColorFromRGB(0xFFCD02); // yellow
    case 128:
      return UIColorFromRGB(0x55EFCB); // green
    case 256:
      return UIColorFromRGB(0x5BCAFF); // green-blue
    case 512:
      return UIColorFromRGB(0x1D77EF); // blue
    case 1024:
      return UIColorFromRGB(0xC643FC); // blue-violin
    case 2048:
      return UIColorFromRGB(0x5856D6); // violin
      
      
    default:
      return [UIColor whiteColor];
      
  }
}

- (UIColor *)numberColorForValue:(NSUInteger)value {
  switch (value) {
    case 1:
    case 2:
    case 4:
      return [UIColor darkGrayColor];
    case 8:
      return [UIColor whiteColor];
    case 16:
      return [UIColor whiteColor];
    case 32:
      return [UIColor whiteColor];
    case 64:
      return [UIColor whiteColor];
    case 128:
      return [UIColor whiteColor];
    case 256:
      return [UIColor whiteColor];
    case 512:
      return [UIColor whiteColor];
    case 1024:
      return [UIColor whiteColor];
    case 2048:
      return [UIColor whiteColor];
    default:
      return [UIColor darkGrayColor];
  }
}


@end
