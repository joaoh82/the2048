//
//  GameModel.h
//  2048
//
//  Created by Nurt  on 3/24/14.
//  Copyright (c) 2014 Nurt Games. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef enum {
  MoveDirectionUp = 0,
  MoveDirectionDown,
  MoveDirectionLeft,
  MoveDirectionRight
} MoveDirection;

@protocol GameModelProtocol

- (void)playerLost;
- (void)playerWonWithTile:(NSIndexPath *)tilePath;
- (void)moveTileFromIndexPath:(NSIndexPath *)fromPath
                  toIndexPath:(NSIndexPath *)toPath
                     newValue:(NSUInteger)value;
- (void)moveTileOne:(NSIndexPath *)startA
            tileTwo:(NSIndexPath *)startB
        toIndexPath:(NSIndexPath *)end
           newValue:(NSUInteger)value;
- (void)insertTileAtIndexPath:(NSIndexPath *)path
                        value:(NSUInteger)value;
- (void)newScore:(NSInteger)score;

@end

@interface GameModel : NSObject

+ (instancetype)gameModelWithDimension:(NSUInteger)dimension
                              winValue:(NSUInteger)value
                              delegate:(id<GameModelProtocol>)delegate;

- (void)insertAtRandomLocationTileWithValue:(NSUInteger)value;

- (void)insertTileWithValue:(NSUInteger)value
                atIndexPath:(NSIndexPath *)path;

- (BOOL)performMoveInDirection:(MoveDirection)direction;

- (BOOL)userHasLost;
- (BOOL)userHasWon;

#pragma mark - Test

- (NSArray *)mergeGroup:(NSArray *)group;

@end

@interface MoveOrder : NSObject

@property (nonatomic) NSInteger source1;
@property (nonatomic) NSInteger source2;
@property (nonatomic) NSInteger destination;
@property (nonatomic) BOOL doubleMove;
@property (nonatomic) NSInteger value;

@end
