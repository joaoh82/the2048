//
//  AppDelegate.m
//  2048
//
//  Created by Nurt  on 16.03.14.
//  Copyright (c) 2014 Nurt Games. All rights reserved.
//

#import "AppDelegate.h"
#import "CJPAdController.h"
#import "GameCenterManager.h"
#import "Chartboost.h"
#import "AppHelper.h"

@interface AppDelegate ()
<
ChartboostDelegate
>

@end

@implementation AppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
  [[GameCenterManager sharedManager] setupManager];
  
  NSString *storyboardName = [AppHelper isPhone] ? @"Main_iPhone" : @"Main_iPad";
  UIStoryboard *storyboard = [UIStoryboard storyboardWithName:storyboardName bundle:nil];
  UINavigationController *navController = (UINavigationController*)[storyboard instantiateInitialViewController];
  
  // init CJPAdController with the nav controller
  _adController = [[CJPAdController sharedManager] initWithContentViewController:navController];
  
  // set the ad controller as the root view controller
  self.window.rootViewController = _adController;
  
  return YES;
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
  [self _setupChartboost];
}

- (void)_setupChartboost {
  Chartboost *cb = [Chartboost sharedChartboost];
  
  cb.appId = CHARBOOST_APP_ID;
  cb.appSignature = CHARBOOST_APP_SIGNATURE;
  
  // Required for use of delegate methods. See "Advanced Topics" section below.
  cb.delegate = self;
  
  // Begin a user session. Must not be dependent on user actions or any prior network requests.
  // Must be called every time your app becomes active.
  [cb startSession];
  
  // Show an interstitial
  [cb showInterstitial];
}

- (void)applicationWillResignActive:(UIApplication *)application {
  
}

- (void)applicationDidEnterBackground:(UIApplication *)application {

}

- (void)applicationWillEnterForeground:(UIApplication *)application {

}

- (void)applicationWillTerminate:(UIApplication *)application {
 
}

@end
